# Input variable definitions
variable "location" {
  description = "The Azure Region in which all resources groups should be created."
  type = string 
}
variable "resource_group_name" {
  description = "The name of the resource group"
  type = string     
}
variable "storage_account_name" {
  description = "The name of the storage account"
  type = string   
}
variable "storage_account_tier" {
  description = "Storage Account Tier"
  type = string   
}
variable "storage_account_replication_type" {
  description = "Storage Account Replication Type"
  type = string   
}
variable "storage_account_kind" {
  description = "Storage Account Kind"
  type = string   
}
variable "static_website_index_document" {
  description = "static website index document"
  type = string   
}
variable "static_website_error_404_document" {
  description = "static website error 404 document"
  type = string   
}
variable "prefixrg" {
  default = "rg-"
}
variable "prefix" {
  default = "tfops"
}

# Azure Subscription
variable "azure-subscription-id" {
  type        = string
  description = "Azure Subscription ID"
  default = "0bf9d854-6513-4c29-9944-6b7477edd3c3" ### ORAMIX

}

# Azure Client-ID
variable "azure-client-id" {
  type        = string
  description = "Azure client-ID"
  default = "ffaec6b2-d955-49e0-9724-65a343281d7c"

}

# Azure Client-Secret
variable "azure-client-secret" {
  type        = string
  description = "Azure client-secret"
  default = "4M58Q~rudyORBubVXXibGm0xjXgouv9g03dr5cPq"

}